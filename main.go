package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/redis/go-redis/v9"
)

func main() {
	fmt.Println("go queue")

	rdb, err := GetRedisClient(context.Background())

	if err != nil {
		fmt.Errorf("err", err.Error())
	}
	// fmt.Println("redis connected")

	go producer(context.Background(), rdb)

	go consumer(context.Background(), "1", rdb)
	// go consumer(context.Background(), "2", rdb)
	// go consumer(context.Background(), "3", rdb)

	time.Sleep(time.Second * 100)
}

func GetRedisClient(ctx context.Context) (*redis.Client, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})

	if err := rdb.Ping(ctx).Err(); err != nil {
		return nil, fmt.Errorf("unable to connect to Redis: %v", err)
	}
	return rdb, nil
}

func producer(ctx context.Context, rdb *redis.Client) {
	s, err := json.Marshal(map[string]interface{}{
		"id":   "481d5aad-811e-404b-a85e-31848fcc5ac6",
		"name": "Testing",
	})
	if err != nil {
		log.Print(err)
	}
	_, err = rdb.LPush(ctx, "queue", string(s)).Result()
	if err != nil {
		fmt.Printf("error pushing to queue: %w", err)
	}
}

func consumer(ctx context.Context, consumerName string, rdb *redis.Client) {
	result, err := rdb.BRPop(ctx, 0, "queue").Result()
	if err != nil {
		fmt.Printf("[%s] error popping from queue: %w\n", consumerName, err)
	}

	fmt.Printf("[%s] received: %v\n", consumerName, result)
}
